package edu.eam.ingesoft.professor.controllers

import edu.eam.ingesoft.professor.config.Groups
import edu.eam.ingesoft.professor.config.Permissions
import edu.eam.ingesoft.professor.config.Routes
import edu.eam.ingesoft.professor.security.Secured
import edu.eam.ingesoft.professor.services.ProfessorService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.PROGRAM_PATH)
class AcademicProgramController {
    @Autowired
    lateinit var professorService: ProfessorService

    @Secured(
        permissions = [Permissions.LIST_OF_TEACHERS_BY_ACADEMIC_PROGRAM],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @GetMapping(Routes.LIST_PROFESSOR_BY_ACADEMIC_PROGRAM_PATH)
    fun findProfessorsByAcademicProgram(@PathVariable("idProgram") idProgram: Long, pageable: Pageable) =
        professorService.findProfessorsByAcademicProgram(idProgram, pageable)
}
