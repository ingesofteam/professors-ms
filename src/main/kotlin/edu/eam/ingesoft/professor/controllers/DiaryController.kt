package edu.eam.ingesoft.professor.controllers

import edu.eam.ingesoft.professor.config.Groups
import edu.eam.ingesoft.professor.config.Permissions
import edu.eam.ingesoft.professor.config.Routes
import edu.eam.ingesoft.professor.security.Secured
import edu.eam.ingesoft.professor.services.DiaryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.PROFESSOR_PATH)
class DiaryController {

    @Autowired
    lateinit var diaryService: DiaryService

    @Secured(
        permissions = [Permissions.CREATE_DIARY_PROFESSOR],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PostMapping(Routes.ADD_DIARY_BY_CODE_PROFESSOR)
    fun createDiary(@PathVariable professorCode: String) =
        diaryService.createDiary(professorCode)

    @GetMapping(Routes.FIND_DIARY_BY_CODE_PROFESSOR_AND_DIARY_ID)
    fun findTheTeachersDiary(@PathVariable professorCode: String, @PathVariable diaryId: Long) =
        diaryService.findTheTeachersDiary(professorCode, diaryId)
}
