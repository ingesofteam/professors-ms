package edu.eam.ingesoft.professor.controllers

import edu.eam.ingesoft.professor.config.Groups
import edu.eam.ingesoft.professor.config.Permissions
import edu.eam.ingesoft.professor.config.Routes
import edu.eam.ingesoft.professor.model.entities.Professor
import edu.eam.ingesoft.professor.security.Secured
import edu.eam.ingesoft.professor.services.ProfessorService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.PROFESSOR_PATH)
class ProfessorController {

    @Autowired
    lateinit var professorService: ProfessorService

    @Secured(permissions = [Permissions.FIND_PROFESSOR], groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR])
    @GetMapping(Routes.FIND_PROFESSOR_PATH)
    fun find(@PathVariable("code") code: String) = professorService.find(code)

    @Secured(
        permissions = [Permissions.CREATE_PROFESSOR],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PostMapping
    fun create(@RequestBody professor: Professor) = professorService.create(professor)

    @Secured(
        permissions = [Permissions.ACTIVATE_PROFESSOR],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PatchMapping(Routes.ACTIVATE_PROFESSOR_PATH)
    fun activate(@PathVariable("code") code: String) = professorService.activate(code)

    @Secured(
        permissions = [Permissions.DEACTIVATE_PROFESSOR],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PatchMapping(Routes.DEACTIVATE_PROFESSOR_PATH)
    fun deactivate(@PathVariable("code") code: String) = professorService.deactivate(code)

    @Secured(permissions = [Permissions.EDIT_PROFESSOR], groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR])
    @PutMapping(Routes.EDIT_PROFESSOR_PATH)
    fun edit(@RequestBody professor: Professor, @PathVariable("code") code: String) =
        professorService.edit(professor, code)
}
