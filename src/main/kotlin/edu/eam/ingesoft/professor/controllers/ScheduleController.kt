package edu.eam.ingesoft.professor.controllers

import edu.eam.ingesoft.professor.config.Groups
import edu.eam.ingesoft.professor.config.Permissions
import edu.eam.ingesoft.professor.config.Routes
import edu.eam.ingesoft.professor.model.requests.CreateScheduleRequest
import edu.eam.ingesoft.professor.model.requests.EditScheduleRequest
import edu.eam.ingesoft.professor.security.Secured
import edu.eam.ingesoft.professor.services.ScheduleService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.PROFESSOR_PATH)
class ScheduleController {

    @Autowired
    lateinit var scheduleService: ScheduleService

    @Secured(permissions = [Permissions.CREATE_SCHEDULE_PROFESSOR], groups = [Groups.PROGRAM_DIRECTOR])
    @PostMapping(Routes.ADD_SCHEDULE_BY_CODE_PROFESSOR_AND_DIARY_ID)
    fun createSchedule(
        @PathVariable professorCode: String,
        @PathVariable diaryId: Long,
        @RequestBody scheduleRequest: CreateScheduleRequest
    ) =
        scheduleService.createSchedule(
            professorCode,
            diaryId,
            scheduleRequest.day,
            scheduleRequest.startAt,
            scheduleRequest.endsAt,
            scheduleRequest.typeId
        )

    @Secured(
        permissions = [Permissions.EDIT_SCHEDULE_PROFESSOR],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PutMapping(Routes.EDIT_SCHEDULE_BY_CODE_PROFESSOR_AND_SCHEDULE_ID)
    fun editSchedule(
        @PathVariable professorCode: String,
        @PathVariable scheduleId: Long,
        @RequestBody scheduleRequest: EditScheduleRequest
    ) =
        scheduleService.editSchedule(
            professorCode,
            scheduleId,
            scheduleRequest.day,
            scheduleRequest.startAt,
            scheduleRequest.endsAt,
            scheduleRequest.typeId
        )

    @Secured(permissions = [Permissions.DELETE_SCHEDULE_PROFESSOR], groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR])
    @DeleteMapping(Routes.DELETE_SCHEDULE_BY_SCHEDULE_ID)
    fun deleteSchedule(@PathVariable scheduleId: Long) =
        scheduleService.deleteSchedule(scheduleId)
}
