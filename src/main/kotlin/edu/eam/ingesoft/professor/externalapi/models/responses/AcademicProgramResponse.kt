package edu.eam.ingesoft.professor.externalapi.models.responses

data class AcademicProgramResponse(
    val id: Long,
    val name: String,
    val snies: String,
    val credits: Int,
    val faculty: FacultyResponse,
    val semesterQty: Int,
    var enabled: Boolean? = false
)
