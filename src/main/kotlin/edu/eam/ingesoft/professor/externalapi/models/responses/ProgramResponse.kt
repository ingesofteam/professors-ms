package edu.eam.ingesoft.professor.externalapi.models.responses

data class ProgramResponse(
    val id: Long,
    val name: String
)
