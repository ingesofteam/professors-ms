package edu.eam.ingesoft.professor.model.entities

import com.fasterxml.jackson.annotation.JsonManagedReference
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = "diaries")
data class Diary(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = 0L,

    @ManyToOne
    @JoinColumn(name = "code_professor", referencedColumnName = "code")
    val professor: Professor,

    @Column(name = "academic_period")
    @NotBlank
    @NotEmpty
    val academicPeriod: String,

    @JsonManagedReference
    @OneToMany(mappedBy = "diary", cascade = [CascadeType.ALL])
    val schedule: MutableList<Schedule>? = null
)
