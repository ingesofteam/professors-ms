package edu.eam.ingesoft.professor.model.entities

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "professor_assignment")
data class ProfessorAssignment(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0L,

    @Column(name = "type_id")
    val typeId: Long,

    val quantity: Int,

    @Column(name = "academic_period_id")
    val academicPeriodId: String,

    @ManyToOne
    @JoinColumn(name = "code_professor", referencedColumnName = "code")
    val professor: Professor
)
