package edu.eam.ingesoft.professor.model.requests

data class FindAssignmentRequest(
    val typeId: Long,
    val quantity: Int
)
