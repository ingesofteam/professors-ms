package edu.eam.ingesoft.professor.repositories

import edu.eam.ingesoft.professor.model.entities.ProfessorAssignment
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface ProfessorAssignmentRepository : PagingAndSortingRepository<ProfessorAssignment, Long> {

    @Query("SELECT p FROM ProfessorAssignment p WHERE p.typeId = :typeId AND p.professor.code = :idProfessor")
    fun findProfessorsWithMoreThanOneAssignmentType(typeId: Long, idProfessor: String): List<ProfessorAssignment>?

    @Query("SELECT p FROM ProfessorAssignment p WHERE p.typeId = :typeId")
    fun findByTypeId(typeId: Long): ProfessorAssignment?

    @Query("SELECT p FROM ProfessorAssignment p WHERE p.professor.code = :idProfessor")
    fun findAssignmentByProfessorId(idProfessor: String): ProfessorAssignment

    @Query("SELECT p FROM ProfessorAssignment p WHERE p.professor.idProgram = :idProgram")
    fun findProfessorAssignmentByProgram(idProgram: Long, pageable: Pageable): Page<ProfessorAssignment>
}
