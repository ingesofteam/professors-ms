package edu.eam.ingesoft.professor.repositories

import edu.eam.ingesoft.professor.model.entities.Schedule
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ScheduleRepository : CrudRepository<Schedule, Long> {
    @Query("SELECT sche FROM Schedule sche WHERE  sche.diary.id =:diaryId")
    fun findByDiaryId(diaryId: Long): MutableList<Schedule>?
}
