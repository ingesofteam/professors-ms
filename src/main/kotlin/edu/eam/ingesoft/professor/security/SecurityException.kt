package edu.eam.ingesoft.professor.security

import java.lang.RuntimeException

class SecurityException(message: String?) : RuntimeException(message)
