package edu.eam.ingesoft.professor.security.authclient

import edu.eam.ingesoft.professor.security.authclient.model.SecurityPayload
import edu.eam.ingesoft.professor.security.authclient.model.Token
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@FeignClient(name = "SecurityClient", url = "\${external-api.security-ms}/security-ms/security/validate-token")
interface SecurityClient {
    @RequestMapping(method = [RequestMethod.POST], path = ["/validate-token"])
    fun validateToken(@RequestBody token: Token): SecurityPayload
}
