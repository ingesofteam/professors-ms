package edu.eam.ingesoft.professor.security.authclient.model

data class Header(
    val name: String,
    val bearer: String
)
