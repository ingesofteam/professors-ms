package edu.eam.ingesoft.professor.services

import edu.eam.ingesoft.professor.exceptions.BusinessException
import edu.eam.ingesoft.professor.externalapi.services.AcademicMSService
import edu.eam.ingesoft.professor.model.entities.ProfessorAssignment
import edu.eam.ingesoft.professor.repositories.ProfessorAssignmentRepository
import edu.eam.ingesoft.professor.repositories.ProfessorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

@Service
class ProfessorAssignmentService {

    @Autowired
    lateinit var professorAssignmentRepository: ProfessorAssignmentRepository

    @Autowired
    lateinit var professorRepository: ProfessorRepository

    @Autowired
    lateinit var academicMSService: AcademicMSService

    fun create(idProfessor: String, typeId: Long, quantity: Int, idAssignment: Long) {
        val academicPeriodId = academicMSService.getAcademicPeriodIdByCurrentDate().id
        academicMSService.getActivity(typeId)

        val professorToFind = professorRepository.findById(idProfessor)

        if (professorToFind.isEmpty) throw BusinessException("Professor not founded", HttpStatus.PRECONDITION_FAILED)

        val professorAssignmentToFind = professorAssignmentRepository.findById(idAssignment)
        if (!professorAssignmentToFind.isEmpty) throw BusinessException(
            "Assignment already exists",
            HttpStatus.PRECONDITION_FAILED
        )

        val professorWithRepeatedAssignment =
            professorAssignmentRepository.findProfessorsWithMoreThanOneAssignmentType(typeId, idProfessor)

        if (professorWithRepeatedAssignment?.isNotEmpty() == true) throw BusinessException(
            "Professor has this assignment already",
            HttpStatus.PRECONDITION_FAILED
        )

        val professorAssignment =
            ProfessorAssignment(typeId = typeId, quantity = quantity, professor = professorToFind.get(), academicPeriodId = academicPeriodId)

        professorAssignmentRepository.save(professorAssignment)
    }

    fun find(idProfessor: String): ProfessorAssignment {

        val professorToFind = professorRepository.findById(idProfessor)
        if (professorToFind.isEmpty) throw BusinessException("Professor was not founded", HttpStatus.NOT_FOUND)

        return professorAssignmentRepository.findAssignmentByProfessorId(idProfessor)
    }

    fun edit(typeId: Long, idProfessor: String, quantity: Int) {

        val professorToFind = professorRepository.findById(idProfessor)
        if (professorToFind.isEmpty) throw BusinessException("Professor not founded", HttpStatus.NOT_FOUND)

        val assignmentById = professorAssignmentRepository.findByTypeId(typeId)
            ?: throw BusinessException("There is not an assignment with this type", HttpStatus.PRECONDITION_FAILED)

        val assignmentToEdit = ProfessorAssignment(assignmentById.id, typeId, quantity, assignmentById.academicPeriodId, assignmentById.professor)
        professorAssignmentRepository.save(assignmentToEdit)
    }

    fun delete(professorId: String, typeId: Long): ProfessorAssignment {

        val professorToFind = professorRepository.findById(professorId)

        if (professorToFind.isEmpty) throw BusinessException("Professor not founded", HttpStatus.NOT_FOUND)

        val professorWithRepeatedAssignment =
            professorAssignmentRepository.findProfessorsWithMoreThanOneAssignmentType(typeId, professorId)

        if (professorWithRepeatedAssignment?.isEmpty() == true) throw BusinessException(
            "Professor has not a register with this type of assignment",
            HttpStatus.PRECONDITION_FAILED
        )

        val assignmentById = professorAssignmentRepository.findByTypeId(typeId)
            ?: throw BusinessException("There is not an assignment with this type", HttpStatus.PRECONDITION_FAILED)

        val assignmentToDelete = ProfessorAssignment(assignmentById.id, typeId, assignmentById.quantity, assignmentById.academicPeriodId, assignmentById.professor)
        professorAssignmentRepository.delete(assignmentToDelete)

        return assignmentToDelete
    }

    fun listAllByIdProgram(idProgram: Long, pageable: Pageable): Page<ProfessorAssignment> {

        academicMSService.findProgram(idProgram)

        return professorAssignmentRepository.findProfessorAssignmentByProgram(idProgram, pageable)
    }
}
