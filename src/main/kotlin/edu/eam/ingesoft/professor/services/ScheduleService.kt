package edu.eam.ingesoft.professor.services

import edu.eam.ingesoft.professor.exceptions.BusinessException
import edu.eam.ingesoft.professor.externalapi.services.AcademicMSService
import edu.eam.ingesoft.professor.model.entities.Schedule
import edu.eam.ingesoft.professor.repositories.DiaryRepository
import edu.eam.ingesoft.professor.repositories.ProfessorRepository
import edu.eam.ingesoft.professor.repositories.ScheduleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import java.time.DayOfWeek
import java.util.Date

@Service
class ScheduleService {

    @Autowired
    lateinit var scheduleRepository: ScheduleRepository

    @Autowired
    lateinit var diaryRepository: DiaryRepository

    @Autowired
    lateinit var professorRepository: ProfessorRepository

    @Autowired
    lateinit var academicMSService: AcademicMSService

    fun createSchedule(
        professorCode: String,
        diaryId: Long,
        day: DayOfWeek,
        startAt: Date,
        endsAt: Date,
        typeId: String
    ) {
        if (!professorRepository.existsById(professorCode)) throw BusinessException(
            "The professor is not founded",
            HttpStatus.NOT_FOUND
        )

        academicMSService.findActivity(typeId)

        if (!diaryRepository.existsById(diaryId)) throw BusinessException(
            "The diary is not founded",
            HttpStatus.NOT_FOUND
        )

        val diary = diaryRepository.findByCodeProfessorAndDiaryId(diaryId, professorCode)
            ?: throw BusinessException("The teachers diary is not assigned", HttpStatus.FORBIDDEN)

        val schedules = scheduleRepository.findByDiaryId(diaryId)

        if (!schedules.isNullOrEmpty()) {
            schedules.forEach {
                if (it.day == day) {
                    when {
                        (it.startAt <= startAt && it.endsAt >= endsAt) -> throw BusinessException(
                            "The Schedule is intersected with other schedule",
                            HttpStatus.PRECONDITION_FAILED
                        )
                    }
                }
            }
        }
        val schedule = Schedule(day = day, startAt = startAt, endsAt = endsAt, typeId = typeId, diary = diary)

        scheduleRepository.save(schedule)
    }

    fun editSchedule(
        professorCode: String,
        scheduleId: Long,
        day: DayOfWeek,
        startAt: Date,
        endsAt: Date,
        typeId: String
    ) {
        if (!professorRepository.existsById(professorCode)) throw BusinessException(
            "The professor is not founded",
            HttpStatus.NOT_FOUND
        )

        if (!scheduleRepository.existsById(scheduleId)) throw BusinessException(
            "The schedule is not founded",
            HttpStatus.NOT_FOUND
        )

        val schedule = scheduleRepository.findById(scheduleId).get()

        if (schedule.diary.professor.code != professorCode) throw BusinessException(
            "The teacher is not assigned the schedule",
            HttpStatus.NOT_FOUND
        )

        if (schedule.day != day || schedule.startAt != startAt || schedule.endsAt != endsAt) {
            val schedules = schedule.diary.id?.let { scheduleRepository.findByDiaryId(it) }

            if (!schedules.isNullOrEmpty()) {
                schedules.forEach {
                    if (it.day == day) {
                        when {
                            (it.startAt <= startAt && it.endsAt >= endsAt) -> throw BusinessException(
                                "The Schedule is intersected with other schedule",
                                HttpStatus.PRECONDITION_FAILED
                            )
                        }
                    }
                }
            }
        }

        academicMSService.findActivity(typeId)

        val scheduleToEdit = Schedule(
            id = schedule.id,
            day = day,
            startAt = startAt,
            endsAt = endsAt,
            typeId = typeId,
            diary = schedule.diary
        )

        scheduleRepository.save(scheduleToEdit)
    }

    fun deleteSchedule(scheduleId: Long) {
        val schedule = scheduleRepository.findById(scheduleId)
        if (schedule.isEmpty) throw BusinessException(
            "The schedule is not founded",
            HttpStatus.NOT_FOUND
        )
        scheduleRepository.delete(schedule.get())
    }
}
