import com.fasterxml.jackson.databind.ObjectMapper
import edu.eam.ingesoft.professor.externalapi.client.AcademicMsClient
import edu.eam.ingesoft.professor.repositories.DiaryRepository
import edu.eam.ingesoft.professor.repositories.ProfessorAssignmentRepository
import edu.eam.ingesoft.professor.repositories.ProfessorRepository
import edu.eam.ingesoft.professor.repositories.ScheduleRepository
import edu.eam.ingesoft.professor.security.authclient.SecurityClient
import edu.eam.ingesoft.professor.security.authclient.model.Header
import edu.eam.ingesoft.professor.security.authclient.model.SecurityPayload
import edu.eam.ingesoft.professor.security.authclient.model.Token
import feign.FeignException
import feign.Request
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.transaction.annotation.Transactional

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Transactional
abstract class CommonTests {

    @Autowired
    protected lateinit var objectMapper: ObjectMapper

    @Autowired
    protected lateinit var professorAssignmentRepository: ProfessorAssignmentRepository

    @Autowired
    protected lateinit var diaryRepository: DiaryRepository

    @Autowired
    protected lateinit var scheduleRepository: ScheduleRepository

    @Autowired
    protected lateinit var professorRepository: ProfessorRepository

    @Autowired
    protected lateinit var mockMvc: MockMvc

    @MockBean
    protected lateinit var academicMsClient: AcademicMsClient

    protected fun createFeignNotFoundException(body: String = "", method: Request.HttpMethod = Request.HttpMethod.POST) =
        FeignException.NotFound(
            "",
            Request.create(method, "null", emptyMap(), "".toByteArray(), null, null),
            body.toByteArray()
        )

    @MockBean
    protected lateinit var securityClient: SecurityClient

    protected fun mockSecurity(
        tokenValue: String = "SecurityToken",
        groups: List<String> = listOf(),
        permissions: List<String> = listOf()
    ): Header {
        val token = Token(tokenValue)
        val payload = SecurityPayload(
            "username",
            groups,
            permissions
        )
        Mockito.`when`(securityClient.validateToken(token)).thenReturn(payload)

        return Header(name = "Authorization", bearer = "Bearer $tokenValue")
    }
}
