package edu.eam.ingesoft.professor.controllers

import CommonTests
import edu.eam.ingesoft.professor.config.Groups
import edu.eam.ingesoft.professor.config.Permissions
import edu.eam.ingesoft.professor.config.Routes
import edu.eam.ingesoft.professor.externalapi.models.responses.AcademicPeriodResponse
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.LocalDate

class DiaryControllerTests : CommonTests() {

    @Test
    @Sql("/queries/diarycontrollertest/create_diary_test.sql")
    fun createDiaryTest() {

        val professorCode: String = "01"
        val academicPeriod: String = "20211"

        val today = LocalDate.now()
        val period = if (today.month.value > 6) "2" else "1"
        Mockito.`when`(academicMsClient.getCurrentPeriod()).thenReturn(
            AcademicPeriodResponse(
                "${today.year}$period",
                "2021-01-01 00:00:00.000",
                "2021-06-30 00:00:00.000"
            )
        )

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_DIARY_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post("${Routes.PROFESSOR_PATH}/${Routes.ADD_DIARY_BY_CODE_PROFESSOR}", professorCode)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val diaries = diaryRepository.findAll().asSequence().toList()
        val diaryToAssert = diaries.find { it.academicPeriod == academicPeriod }

        Assert.assertNotNull(diaryToAssert)
        Assert.assertEquals(academicPeriod, diaryToAssert?.academicPeriod)
    }

    @Test
    fun createDiaryWhenProfessorNotExistTest() {

        val professorCode: String = "01"

        val today = LocalDate.now()
        val period = if (today.month.value > 6) "2" else "1"
        Mockito.`when`(academicMsClient.getCurrentPeriod()).thenReturn(
            AcademicPeriodResponse(
                "${today.year}$period",
                "2021-01-01 00:00:00.000",
                "2021-06-30 00:00:00.000"
            )
        )

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_DIARY_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post("${Routes.PROFESSOR_PATH}/${Routes.ADD_DIARY_BY_CODE_PROFESSOR}", professorCode)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The professor is not founded")))
    }

    @Test
    @Sql("/queries/diarycontrollertest/create_diary_when_academic_period_is_empty_test.sql")
    fun createDiaryWhenAcademicPeriodIsWrong() {
        val professorCode: String = "01"

        val today = LocalDate.now()
        Mockito.`when`(academicMsClient.getCurrentPeriod()).thenThrow(createFeignNotFoundException())

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_DIARY_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post("${Routes.PROFESSOR_PATH}/${Routes.ADD_DIARY_BY_CODE_PROFESSOR}", professorCode)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("Academic period was not founded")
                )
            )
    }

    @Test
    @Sql("/queries/diarycontrollertest/find_the_teachers_diary.sql")
    fun findTheTeachersDiaryTest() {
        val professorCode: String = "01"
        val diaryId: Long = 100L

        val request = MockMvcRequestBuilders
            .get("${Routes.PROFESSOR_PATH}/${Routes.FIND_DIARY_BY_CODE_PROFESSOR_AND_DIARY_ID}", professorCode, diaryId)
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.`is`(100)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.professor.code", Matchers.`is`(professorCode)))
    }

    @Test
    @Sql("/queries/diarycontrollertest/find_the_teachers_diary_when_the_professor_not_exist_test.sql")
    fun findTheTeachersDiaryWhenTheProfessorNotExistTest() {
        val professorCode: String = "01"
        val diaryId: Long = 101L

        val request = MockMvcRequestBuilders
            .get("${Routes.PROFESSOR_PATH}/${Routes.FIND_DIARY_BY_CODE_PROFESSOR_AND_DIARY_ID}", professorCode, diaryId)
            .contentType(MediaType.APPLICATION_JSON_VALUE)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The professor is not founded")))
    }

    @Test
    @Sql("/queries/diarycontrollertest/find_the_teachers_diary_when_the_diary_not_exist_test.sql")
    fun findTheTeachersDiaryWhenTheDiaryNotExistTest() {
        val professorCode: String = "01"
        val diaryId: Long = 80L

        val request = MockMvcRequestBuilders
            .get("${Routes.PROFESSOR_PATH}/${Routes.FIND_DIARY_BY_CODE_PROFESSOR_AND_DIARY_ID}", professorCode, diaryId)
            .contentType(MediaType.APPLICATION_JSON_VALUE)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The diary is not founded")))
    }

    @Test
    @Sql("/queries/diarycontrollertest/find_the_teachers_diary_when_the_diary_is_not_assigned.sql")
    fun findTheTeachersDiaryWhenTheDiaryIsNotAssigned() {
        val professorCode: String = "01"
        val diaryId: Long = 104L

        val request = MockMvcRequestBuilders
            .get("${Routes.PROFESSOR_PATH}/${Routes.FIND_DIARY_BY_CODE_PROFESSOR_AND_DIARY_ID}", professorCode, diaryId)
            .contentType(MediaType.APPLICATION_JSON_VALUE)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("The teachers diary is not assigned")
                )
            )
    }
}
