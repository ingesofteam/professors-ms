package edu.eam.ingesoft.professor.controllers

import CommonTests
import edu.eam.ingesoft.professor.config.Routes
import edu.eam.ingesoft.professor.externalapi.models.responses.AcademicProgramResponse
import edu.eam.ingesoft.professor.externalapi.models.responses.FacultyResponse
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class FacultyControllerTest : CommonTests() {
    @Test
    @Sql("/queries/ProfessorControllerTest/findProfessorsByFacultyTest.sql")
    fun listProfessorsByFacultyTest() {
        val faculty = FacultyResponse(1L, "Ingenieria", true)
        val academicPrograms = listOf<AcademicProgramResponse>(
            AcademicProgramResponse(1L, "Software", "202020", 85, faculty, 9, true),
            AcademicProgramResponse(2L, "Mecatronica", "303030", 95, faculty, 10, true)
        )

        Mockito.`when`(academicMsClient.listProgramsByFaculty(1L)).thenReturn(academicPrograms)

        val request = MockMvcRequestBuilders
            .get("${Routes.FACULTY_PATH}${Routes.LIST_PROFESSORS_BY_FACULTY_PATH}", 1L)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(3)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[1].id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[2].id_program", Matchers.`is`(2)))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/findProfessorsByFacultyTest.sql")
    fun listProfessorsByFacultyThatDoesntExistTest() {
        Mockito.`when`(academicMsClient.listProgramsByFaculty(1L)).thenThrow(createFeignNotFoundException())

        val request = MockMvcRequestBuilders
            .get("${Routes.FACULTY_PATH}${Routes.LIST_PROFESSORS_BY_FACULTY_PATH}", 1L)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Faculty Not Found")))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/findProfessorsByFacultyTest.sql")
    fun listProfessorsByFacultySizeTest() {
        val faculty = FacultyResponse(1L, "Ingenieria", true)
        val academicPrograms = listOf<AcademicProgramResponse>(
            AcademicProgramResponse(1L, "Software", "202020", 85, faculty, 9, true),
            AcademicProgramResponse(2L, "Mecatronica", "303030", 95, faculty, 10, true)
        )

        Mockito.`when`(academicMsClient.listProgramsByFaculty(1L)).thenReturn(academicPrograms)

        val request = MockMvcRequestBuilders
            .get("${Routes.FACULTY_PATH}${Routes.LIST_PROFESSORS_BY_FACULTY_PATH}?size=1", 1L)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].id_program", Matchers.`is`(1)))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/findProfessorsByFacultyTest.sql")
    fun listProfessorsByFacultyPageTest() {
        val faculty = FacultyResponse(1L, "Ingenieria", true)
        val academicPrograms = listOf<AcademicProgramResponse>(
            AcademicProgramResponse(1L, "Software", "202020", 85, faculty, 9, true),
            AcademicProgramResponse(2L, "Mecatronica", "303030", 95, faculty, 10, true)
        )

        Mockito.`when`(academicMsClient.listProgramsByFaculty(1L)).thenReturn(academicPrograms)

        val request = MockMvcRequestBuilders
            .get("${Routes.FACULTY_PATH}${Routes.LIST_PROFESSORS_BY_FACULTY_PATH}?size=2&page=1", 1L)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].id_program", Matchers.`is`(2)))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/findProfessorsByFacultyTest.sql")
    fun listProfessorsByFacultySortTest() {
        val faculty = FacultyResponse(1L, "Ingenieria", true)
        val academicPrograms = listOf<AcademicProgramResponse>(
            AcademicProgramResponse(1L, "Software", "202020", 85, faculty, 9, true),
            AcademicProgramResponse(2L, "Mecatronica", "303030", 95, faculty, 10, true)
        )

        Mockito.`when`(academicMsClient.listProgramsByFaculty(1L)).thenReturn(academicPrograms)

        val request = MockMvcRequestBuilders
            .get("${Routes.FACULTY_PATH}${Routes.LIST_PROFESSORS_BY_FACULTY_PATH}?sort=name,asc", 1L)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(3)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].name", Matchers.`is`("Allen")))
            .andExpect(MockMvcResultMatchers.jsonPath("content[1].name", Matchers.`is`("Cross")))
            .andExpect(MockMvcResultMatchers.jsonPath("content[2].name", Matchers.`is`("Leenale")))
    }
}
