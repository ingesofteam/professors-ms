package edu.eam.ingesoft.professor.controllers

import CommonTests
import edu.eam.ingesoft.professor.config.Groups
import edu.eam.ingesoft.professor.config.Permissions
import edu.eam.ingesoft.professor.config.Routes
import edu.eam.ingesoft.professor.externalapi.models.responses.AcademicPeriodResponse
import edu.eam.ingesoft.professor.externalapi.models.responses.ActivityResponse
import edu.eam.ingesoft.professor.externalapi.models.responses.ProgramResponse
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.LocalDate

class ProfessorAssignmentControllerTest : CommonTests() {

    @Test
    @Sql("/queries/assignmentControllerTest/createProfessorAssignmentTest.sql")
    fun createAssignmentTest() {
        val typeId: Long = 1L
        val quantity: Int = 1
        val assignmentId: Long = 1L
        val professorId: String = "100"
        val today = LocalDate.now()
        val period = if (today.month.value > 6) "2" else "1"

        Mockito.`when`(academicMsClient.getActivity(typeId)).thenReturn(ActivityResponse(1L, "name1", "desc1"))
        Mockito.`when`(academicMsClient.getCurrentPeriod()).thenReturn(AcademicPeriodResponse("${today.year}$period", "2021-01-01 00:00:00.000", "2021-06-30 00:00:00.000"))

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.ADD_TEACHER_ASSIGNMENT)
        )

        val jsonBody =
            """{
        "type_id" : "$typeId",
        "quantity": "$quantity",
        "assignment_id" : "$assignmentId"
        }"""

        val request = MockMvcRequestBuilders
            .post("${Routes.PROFESSOR_PATH}/${Routes.CREATE_ASSIGNMENT_BY_PROFESSOR}", professorId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val assignmentList = professorAssignmentRepository.findAll().toList()

        val professorAssignmentToAssert = assignmentList.elementAt(0)

        Assert.assertNotNull(professorAssignmentToAssert)
        Assert.assertEquals(professorId, professorAssignmentToAssert?.professor?.code)
    }

    @Test
    @Sql("/queries/assignmentControllerTest/createAssignmentWithNotActivityInAcademicMsTest.sql")
    fun createAssignmentWithNotActivityInAcademicMsTest() {

        val typeId: Long = 1L
        val quantity: Int = 1
        val assignmentId: Long = 1L
        val academicPeriodId = "20202"
        val professorId: String = "1"
        val today = LocalDate.now()
        val period = if (today.month.value > 6) "2" else "1"

        Mockito.`when`(academicMsClient.getActivity(typeId)).thenThrow(createFeignNotFoundException())
        Mockito.`when`(academicMsClient.getCurrentPeriod()).thenReturn(AcademicPeriodResponse("${today.year}$period", "2021-01-01 00:00:00.000", "2021-06-30 00:00:00.000"))

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.ADD_TEACHER_ASSIGNMENT)
        )

        val jsonBody =
            """{
        "type_id" : "$typeId",
        "quantity": "$quantity",
        "assignment_id" : "$assignmentId",
        "academic_period_id" : "$academicPeriodId"
        }"""
        val request = MockMvcRequestBuilders
            .post("${Routes.PROFESSOR_PATH}/${Routes.CREATE_ASSIGNMENT_BY_PROFESSOR}", professorId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
    }

    @Test
    @Sql("/queries/assignmentControllerTest/createAssignmentWithNotActivityInAcademicMsTest.sql")
    fun createAssignmentWithNotAcademicPeriodInAcademicMsTest() {

        val typeId: Long = 1L
        val quantity: Int = 1
        val assignmentId: Long = 1L
        val professorId: String = "1"

        Mockito.`when`(academicMsClient.getActivity(typeId)).thenReturn(ActivityResponse(1L, "name1", "desc1"))
        Mockito.`when`(academicMsClient.getCurrentPeriod()).thenThrow(createFeignNotFoundException())

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.ADD_TEACHER_ASSIGNMENT)
        )

        val jsonBody =
            """{
        "type_id" : "$typeId",
        "quantity": "$quantity",
        "assignment_id" : "$assignmentId"
        }"""
        val request = MockMvcRequestBuilders
            .post("${Routes.PROFESSOR_PATH}/${Routes.CREATE_ASSIGNMENT_BY_PROFESSOR}", professorId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
    }

    @Test
    @Sql("/queries/assignmentControllerTest/createRepeatedAssignment.sql")
    fun createAssignmentAlreadyExistTest() {
        val typeId: Long = 1L
        val quantity: Int = 1
        val assignmentId: Long = 1L
        val professorId: String = "1"
        val today = LocalDate.now()
        val period = if (today.month.value > 6) "2" else "1"

        Mockito.`when`(academicMsClient.getActivity(typeId)).thenReturn(ActivityResponse(1L, "name1", "desc1"))
        Mockito.`when`(academicMsClient.getCurrentPeriod()).thenReturn(AcademicPeriodResponse("${today.year}$period", "2021-01-01 00:00:00.000", "2021-06-30 00:00:00.000"))

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.ADD_TEACHER_ASSIGNMENT)
        )

        val jsonBody =
            """{
        "type_id" : "$typeId",
        "quantity": "$quantity",
        "assignment_id" : "$assignmentId"
        }"""
        val request = MockMvcRequestBuilders

            .post("${Routes.PROFESSOR_PATH}/${Routes.CREATE_ASSIGNMENT_BY_PROFESSOR}", professorId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Assignment already exists")))
    }

    @Test
    @Sql("/queries/assignmentControllerTest/professorHasMoreThanOneAssignmentTest.sql")
    fun professorHasMoreThanOneAssignmentTest() {
        val typeId: Long = 3L
        val quantity: Int = 2
        val assignmentId: Long = 11L
        val professorId: String = "3"
        val today = LocalDate.now()
        val period = if (today.month.value > 6) "2" else "1"

        Mockito.`when`(academicMsClient.getActivity(typeId)).thenReturn(ActivityResponse(1L, "name1", "desc1"))
        Mockito.`when`(academicMsClient.getCurrentPeriod()).thenReturn(AcademicPeriodResponse("${today.year}$period", "2021-01-01 00:00:00.000", "2021-06-30 00:00:00.000"))

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.ADD_TEACHER_ASSIGNMENT)
        )

        val jsonBody =
            """{
        "type_id" : "$typeId",
        "quantity": "$quantity",
        "assignment_id" : "$assignmentId"
        }"""
        val request = MockMvcRequestBuilders

            .post("${Routes.PROFESSOR_PATH}/${Routes.CREATE_ASSIGNMENT_BY_PROFESSOR}", professorId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("Professor has this assignment already")
                )
            )
    }

    @Test
    fun createAssignmentAndProfessorNotExistTest() {
        val typeId: Long = 1L
        val quantity: Int = 1
        val assignmentId: Long = 1L
        val professorId: String = "1"
        val today = LocalDate.now()
        val period = if (today.month.value > 6) "2" else "1"

        Mockito.`when`(academicMsClient.getActivity(typeId)).thenReturn(ActivityResponse(1L, "name1", "desc1"))
        Mockito.`when`(academicMsClient.getCurrentPeriod()).thenReturn(AcademicPeriodResponse("${today.year}$period", "2021-01-01 00:00:00.000", "2021-06-30 00:00:00.000"))

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.ADD_TEACHER_ASSIGNMENT)
        )

        val jsonBody =
            """{
        "type_id" : "$typeId",
        "quantity": "$quantity",
        "assignment_id" : "$assignmentId"
        }"""
        val request = MockMvcRequestBuilders

            .post("${Routes.PROFESSOR_PATH}/${Routes.CREATE_ASSIGNMENT_BY_PROFESSOR}", professorId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor not founded")))
    }

    @Test
    @Sql("/queries/assignmentControllerTest/findProfessorAssignmentTest.sql")
    fun findProfessorAssignmentTest() {

        val professorId: String = "1"

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.FIND_PROFESSOR_ASSIGNMENT)
        )

        val request = MockMvcRequestBuilders
            .get("${Routes.PROFESSOR_PATH}/${Routes.FIND_ASSIGNMENT_BY_PROFESSOR}", professorId)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val assignments = professorAssignmentRepository.findAll().asSequence().toList()

        val assignmentsToAssert = assignments.find { it.id == 1L }

        Assert.assertNotNull(assignmentsToAssert)
        Assert.assertEquals("andres", assignmentsToAssert?.professor?.name)
        Assert.assertEquals(1, assignmentsToAssert?.quantity)
    }

    @Test
    fun findProfessorAssignmentNullTest() {

        val professorId: String = "1"

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.FIND_PROFESSOR_ASSIGNMENT)
        )

        val request = MockMvcRequestBuilders
            .get("${Routes.PROFESSOR_PATH}/${Routes.FIND_ASSIGNMENT_BY_PROFESSOR}", professorId)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor was not founded")))
    }

    @Test
    @Sql("/queries/assignmentControllerTest/editProfessorAssignmentTest.sql")
    fun editProfessorAssignmentTest() {

        val typeId: Long = 1L
        val quantity: Int = 222
        val professorId: String = "1"

        val jsonBody =
            """{
        "type_id" : "$typeId",
        "quantity": "$quantity"
        }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.EDIT_PROFESSOR_ASSIGNMENT)
        )

        val request = MockMvcRequestBuilders
            .put("${Routes.PROFESSOR_PATH}/${Routes.EDIT_ASSIGNMENT_BY_PROFESSOR}", professorId, typeId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val assignments = professorAssignmentRepository.findAll().asSequence().toList()

        val assignmentsToAssert = assignments.find { it.id == 1L }

        Assert.assertNotNull(assignmentsToAssert)
        Assert.assertEquals("andres", assignmentsToAssert?.professor?.name)
        Assert.assertEquals(222, assignmentsToAssert?.quantity)
    }

    @Test
    @Sql("/queries/assignmentControllerTest/editProfessorNotFoundTest.sql")
    fun editProfessorNotFoundTest() {

        val typeId: Long = 1L
        val quantity: Int = 222
        val professorId: String = "23"

        val jsonBody =
            """{
        "type_id" : "$typeId",
        "quantity": "$quantity"
        }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.EDIT_PROFESSOR_ASSIGNMENT)
        )

        val request = MockMvcRequestBuilders
            .put("${Routes.PROFESSOR_PATH}/${Routes.EDIT_ASSIGNMENT_BY_PROFESSOR}", professorId, typeId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor not founded")))
    }

    @Test
    @Sql("/queries/assignmentControllerTest/assignmentWithWrongTypeTest.sql")
    fun assignmentWithWrongTypeTest() {

        val typeId: Long = 1L
        val quantity: Int = 222
        val professorId: String = "1"

        val jsonBody =
            """{
        "type_id" : "$typeId",
        "quantity": "$quantity"
        }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.EDIT_PROFESSOR_ASSIGNMENT)
        )

        val request = MockMvcRequestBuilders
            .put("${Routes.PROFESSOR_PATH}/${Routes.EDIT_ASSIGNMENT_BY_PROFESSOR}", professorId, typeId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("There is not an assignment with this type")))
    }

    @Test
    @Sql("/queries/assignmentControllerTest/DeleteProfessorAssignmentTest.sql")
    fun deleteProfessorAssignmentTest() {

        val typeId: Long = 1L
        val professorId: String = "1"

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.DELETE_PROFESSORS_ASSIGNMENT)
        )

        val request = MockMvcRequestBuilders
            .delete("${Routes.PROFESSOR_PATH}/${Routes.DELETE_ASSIGNMENT_BY_PROFESSOR}", typeId, professorId)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val assignments = professorAssignmentRepository.findAll().asSequence().toList()

        val assignmentsToAssert = assignments.find { it.id == 1L }

        Assert.assertNull(assignmentsToAssert)
    }

    @Test
    @Sql("/queries/assignmentControllerTest/DeleteProfessorNotFoundTest.sql")
    fun deleteProfessorNotFoundTest() {

        val typeId: Long = 1L
        val professorId: String = "12"

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.DELETE_PROFESSORS_ASSIGNMENT)
        )

        val request = MockMvcRequestBuilders
            .delete("${Routes.PROFESSOR_PATH}/${Routes.DELETE_ASSIGNMENT_BY_PROFESSOR}", typeId, professorId)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor not founded")))
    }

    @Test
    @Sql("/queries/assignmentControllerTest/DeleteAssignmentAndHasNotTypeTest.sql")
    fun deleteAssignmentAndHasNotTypeTest() {

        val typeId: Long = 3L
        val professorId: String = "7"

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.DELETE_PROFESSORS_ASSIGNMENT)
        )

        val request = MockMvcRequestBuilders
            .delete("${Routes.PROFESSOR_PATH}/${Routes.DELETE_ASSIGNMENT_BY_PROFESSOR}", professorId, typeId)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("Professor has not a register with this type of assignment")
                )
            )
    }

    @Test
    @Sql("/queries/assignmentControllerTest/getProfessorsAssignmentsByAcademicProgramTest.sql")
    fun getProfessorsAssignmentsByAcademicProgramTest() {
        Mockito.`when`(academicMsClient.getProgram(1)).thenReturn(ProgramResponse(1, "Software"))

        val idProgram: Long = 1L

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.FIND_PROFESSOR_ASSIGNMENT_BY_PROGRAM_ID)
        )

        val request = MockMvcRequestBuilders
            .get("${Routes.PROFESSOR_PATH}/${Routes.PROFESSOR_ASSIGNMENT_BY_PROGRAM}", idProgram)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(5)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].professor.id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[1].professor.id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[2].professor.id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[3].professor.id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[4].professor.id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("pageable.page_number", Matchers.`is`(0)))
            .andExpect(MockMvcResultMatchers.jsonPath("size", Matchers.`is`(5)))
            .andExpect(MockMvcResultMatchers.jsonPath("sort.sorted", Matchers.`is`(false)))
    }

    @Test
    fun getProfessorAssignmentByAcademicProgramWithoutAcademicProgramTest() {
        Mockito.`when`(academicMsClient.getProgram(8)).thenThrow(createFeignNotFoundException())
        val idProgram: Long = 8L

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER
            ),
            permissions = listOf(Permissions.FIND_PROFESSOR_ASSIGNMENT_BY_PROGRAM_ID)
        )

        val request = MockMvcRequestBuilders
            .get("${Routes.PROFESSOR_PATH}/${Routes.PROFESSOR_ASSIGNMENT_BY_PROGRAM}", idProgram)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The program doesn't exist")))
    }
}
