package edu.eam.ingesoft.professor.controllers

import CommonTests
import edu.eam.ingesoft.professor.config.Groups
import edu.eam.ingesoft.professor.config.Permissions
import edu.eam.ingesoft.professor.config.Routes
import edu.eam.ingesoft.professor.externalapi.models.responses.ProgramResponse
import edu.eam.ingesoft.professor.model.entities.Professor
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class ProfessorControllerTest : CommonTests() {

    @Test
    @Sql("/queries/ProfessorControllerTest/editProfessorTest.sql")
    fun editProfessorTest() {
        val professor = Professor("01", "1010", "Allen", "Walker", "mCross@eam.edu.co", "3203021234", "10", 0, "Professor")

        Mockito.`when`(academicMsClient.getProgram(1)).thenReturn(ProgramResponse(1, "Software"))

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.EDIT_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .put(Routes.PROFESSOR_PATH + "/01")
            .content(objectMapper.writeValueAsString(professor))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val professors = professorRepository.findAll().asSequence().toList()

        val professorToAssert = professors.find { it.code == professor.code }

        Assert.assertNotNull(professorToAssert)
        Assert.assertEquals(professor.name, professorToAssert?.name)
        Assert.assertEquals(professor.lastName, professorToAssert?.lastName)
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/editProfessorTest.sql")
    fun editProfessorWithoutPermissionsTest() {
        val professor = Professor("01", "1010", "Allen", "Walker", "mCross@eam.edu.co", "3203021234", "10", 0, "Professor")

        Mockito.`when`(academicMsClient.getProgram(1)).thenReturn(ProgramResponse(1, "Software"))

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf("._.")
        )

        val request = MockMvcRequestBuilders
            .put(Routes.PROFESSOR_PATH + "/01")
            .content(objectMapper.writeValueAsString(professor))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("This User has no access")))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/editProfessorTest.sql")
    fun editProfessorWithoutHeaderTest() {
        val professor = Professor("01", "1010", "Allen", "Walker", "mCross@eam.edu.co", "3203021234", "10", 0, "Professor")

        Mockito.`when`(academicMsClient.getProgram(1)).thenReturn(ProgramResponse(1, "Software"))

        val request = MockMvcRequestBuilders
            .put(Routes.PROFESSOR_PATH + "/01")
            .content(objectMapper.writeValueAsString(professor))
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Token required")))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/editProfessorTest.sql")
    fun editProfessorWithWrongHeaderTest() {
        val professor = Professor("01", "1010", "Allen", "Walker", "mCross@eam.edu.co", "3203021234", "10", 0, "Professor")

        Mockito.`when`(academicMsClient.getProgram(1)).thenReturn(ProgramResponse(1, "Software"))

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf("Prueba")
        )

        val request = MockMvcRequestBuilders
            .put(Routes.PROFESSOR_PATH + "/01")
            .content(objectMapper.writeValueAsString(professor))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, "Wrong Token")

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Invalid Token")))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/editProfessorWithAlreadyUsedIdentificationNumberTest.sql")
    fun editProfessorWithAlreadyUsedIdentificationNumberTest() {
        val professor = Professor("01", "2020", "Allen", "Walker", "mCross@eam.edu.co", "3203021234", "10", 0, "Professor")

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.EDIT_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .put(Routes.PROFESSOR_PATH + "/01")
            .content(objectMapper.writeValueAsString(professor))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor already created with that identification number")))
    }

    @Test
    fun editProfessorThatDoNotExist() {
        val professor = Professor("500", "2020", "Allen", "Walker", "mCross@eam.edu.co", "3203021234", "10", 0, "Professor")

        Mockito.`when`(academicMsClient.getProgram(1)).thenThrow(createFeignNotFoundException())

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.EDIT_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .put(Routes.PROFESSOR_PATH + "/500")
            .content(objectMapper.writeValueAsString(professor))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor doesn't exist")))
    }

    @Test
    fun createProfessorTest() {
        val professor = Professor("01", "1010", "Mariam", "Cross", "mCross@eam.edu.co", "3203021234", "10", 0, "Professor")

        Mockito.`when`(academicMsClient.getProgram(professor.idProgram)).thenReturn(ProgramResponse(1L, "name1"))

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post(Routes.PROFESSOR_PATH)
            .content(objectMapper.writeValueAsString(professor))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val professors = professorRepository.findAll().asSequence().toList()

        val professorToAssert = professors.find { it.name == professor.name }
        Assert.assertNotNull(professorToAssert)
        Assert.assertEquals(professor.name, professorToAssert?.name)
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/createProfessorWithAlreadyUsedCodeTest.sql")
    fun createProfessorWithAlreadyUsedCodeTest() {
        val professor = Professor("01", "1010", "Mariam", "Cross", "mCross@eam.edu.co", "3203021234", "10", 0, "Professor")

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post(Routes.PROFESSOR_PATH)
            .content(objectMapper.writeValueAsString(professor))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor already created with that code")))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/createProfessorWithAlreadyUsedIdentificationNumberTest.sql")
    fun createProfessorWithAlreadyUsedIdentificationNumberTest() {
        val professor = Professor("01", "1010", "Mariam", "Cross", "mCross@eam.edu.co", "3203021234", "10", 0, "Professor")

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post(Routes.PROFESSOR_PATH)
            .content(objectMapper.writeValueAsString(professor))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor already created with that identification number")))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/activateProfessorTest.sql")
    fun activateProfessorTest() {
        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.ACTIVATE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .patch(Routes.PROFESSOR_PATH + "/01/activate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val professors = professorRepository.findAll().asSequence().toList()

        val professorToAssert = professors.find { it.code == "01" }

        Assert.assertNotNull(professorToAssert)
        Assert.assertEquals(true, professorToAssert?.enabled)
    }

    @Test
    fun activateProfessorNotExistTest() {
        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.ACTIVATE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .patch(Routes.PROFESSOR_PATH + "/500/activate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor Not Found")))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/activateProfessorAlreadyActivedTest.sql")
    fun activateProfessorAlreadyActivedTest() {
        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.ACTIVATE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .patch(Routes.PROFESSOR_PATH + "/01/activate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor already activated")))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/deactivateProfessorTest.sql")
    fun deactivateProfessorTest() {
        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.DEACTIVATE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .patch(Routes.PROFESSOR_PATH + "/01/deactivate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val professors = professorRepository.findAll().asSequence().toList()

        val professorToAssert = professors.find { it.code == "01" }

        Assert.assertNotNull(professorToAssert)
        Assert.assertEquals(false, professorToAssert?.enabled)
    }

    @Test
    fun deactivateProfessorNotExistTest() {
        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.DEACTIVATE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .patch(Routes.PROFESSOR_PATH + "/500/deactivate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor Not Found")))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/deactivateProfessorAlreadyDeactivedTest.sql")
    fun deactivateProfessorAlreadyDeactivedTest() {
        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.DEACTIVATE_PROFESSOR)
        )
        val request = MockMvcRequestBuilders
            .patch(Routes.PROFESSOR_PATH + "/01/deactivate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor already deactivated")))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/findProfessorTest.sql")
    fun findProfessorTest() {
        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.FIND_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .get(Routes.PROFESSOR_PATH + "/01")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.code", Matchers.`is`("01")))
            .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.`is`("Mariam")))
    }

    @Test
    fun findProfessorThatNotExistTest() {
        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.FIND_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .get(Routes.PROFESSOR_PATH + "/500")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor Not Found")))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/edit_professor_by_exist_test.sql")
    fun editProfessorByExistTest() {

        val professor = Professor("1", "2020", "Allen", "Walker", "mCross@eam.edu.co", "3203021234", "10", 1, "Professor")

        Mockito.`when`(academicMsClient.getProgram(1)).thenReturn(ProgramResponse(1, "Software"))

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.EDIT_PROFESSOR)
        )

        val idProfessor: Long = 1

        val request = MockMvcRequestBuilders
            .put("${Routes.PROFESSOR_PATH}/${Routes.EDIT_PROFESSOR_PATH}", idProfessor)
            .content(objectMapper.writeValueAsString(professor))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val professors = professorRepository.findAll().asSequence().toList()

        val professorToAssert = professors.find { it.code == professor.code }

        Assert.assertNotNull(professorToAssert)
        Assert.assertEquals(professor.name, professorToAssert?.name)
        Assert.assertEquals(professor.lastName, professorToAssert?.lastName)
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/editProfessorTest.sql")
    fun editProfessorNotExistTest() {
        val professor = Professor("01", "1010", "Allen", "Walker", "mCross@eam.edu.co", "3203021234", "10", 1, "Professor")

        Mockito.`when`(academicMsClient.getProgram(1)).thenThrow(createFeignNotFoundException())

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.EDIT_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .put("${Routes.PROFESSOR_PATH}/${Routes.EDIT_PROFESSOR_PATH}", "01")
            .content(objectMapper.writeValueAsString(professor))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The program doesn't exist")))
    }

    @Test
    fun createProfessorTestWhenAcademicProgramDoesNotExist() {
        val professor = Professor("01", "1010", "Mariam", "Cross", "mCross@eam.edu.co", "3203021234", "10", 0, "Professor")

        Mockito.`when`(academicMsClient.getProgram(professor.idProgram)).thenThrow(createFeignNotFoundException())

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post(Routes.PROFESSOR_PATH)
            .content(objectMapper.writeValueAsString(professor))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
    }
}
