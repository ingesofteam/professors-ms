package edu.eam.ingesoft.professor.controllers

import CommonTests
import edu.eam.ingesoft.professor.config.Groups
import edu.eam.ingesoft.professor.config.Permissions
import edu.eam.ingesoft.professor.config.Routes
import edu.eam.ingesoft.professor.externalapi.models.responses.ActivityResponse
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class ScheduleControllerTests : CommonTests() {
    @Test
    @Sql("/queries/schedulecontrollertest/create_schedule_test.sql")
    fun createScheduleTest() {
        val activity = ActivityResponse(123456L, "actividad ejemplo", "descripcion ejemplo")

        Mockito.`when`(academicMsClient.findActivity("123456")).thenReturn(activity)

        val jsonBody =
            """{
                "day": "TUESDAY",
                "start_at": "2021-03-09 10:00:00.0",
                "ends_at": "2021-03-09 12:00:00.0",
                "type_id": "123456"
            }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post("${Routes.PROFESSOR_PATH}/${Routes.ADD_SCHEDULE_BY_CODE_PROFESSOR_AND_DIARY_ID}", "01", 1L)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val schedules = scheduleRepository.findAll().asSequence().toList()

        val scheduleToAssert = schedules.find { it.typeId == "123456" }

        Assert.assertNotNull(scheduleToAssert)
        Assert.assertEquals("01", scheduleToAssert?.diary?.professor?.code)
        Assert.assertEquals("tuesday", scheduleToAssert?.day.toString().toLowerCase())
        Assert.assertEquals("123456", scheduleToAssert?.typeId)
    }

    @Test
    @Sql("/queries/schedulecontrollertest/create_schedule_when_professor_not_exist_test.sql")
    fun createScheduleWhenProfessorNotExistTest() {
        val activity = ActivityResponse(123456L, "actividad ejemplo", "descripcion ejemplo")

        Mockito.`when`(academicMsClient.findActivity("123456")).thenReturn(activity)

        val jsonBody =
            """{
                "day": "TUESDAY",
                "start_at": "2021-03-09 10:00:00.0",
                "ends_at": "2021-03-09 12:00:00.0",
                "type_id": "tipo 1"
            }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post("${Routes.PROFESSOR_PATH}/${Routes.ADD_SCHEDULE_BY_CODE_PROFESSOR_AND_DIARY_ID}", "02", 95L)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The professor is not founded")))
    }

    @Test
    @Sql("/queries/schedulecontrollertest/create_schedule_test.sql")
    fun createScheduleWhenActivityNotExistTest() {
        val jsonBody =
            """{
                "day": "TUESDAY",
                "start_at": "2021-03-09 10:00:00.0",
                "ends_at": "2021-03-09 12:00:00.0",
                "type_id": "1"
            }"""

        Mockito.`when`(academicMsClient.findActivity("1")).thenThrow(createFeignNotFoundException())

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post("${Routes.PROFESSOR_PATH}/${Routes.ADD_SCHEDULE_BY_CODE_PROFESSOR_AND_DIARY_ID}", "01", 95L)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Activity Not Found")))
    }

    @Test
    @Sql("/queries/schedulecontrollertest/create_schedule_when_diary_not_exist_test.sql")
    fun createScheduleWhenDiaryNotExistTest() {
        val activity = ActivityResponse(123456L, "actividad ejemplo", "descripcion ejemplo")

        Mockito.`when`(academicMsClient.findActivity("123456")).thenReturn(activity)

        val jsonBody =
            """{
                "day": "TUESDAY",
                "start_at": "2021-03-09 10:00:00.0",
                "ends_at": "2021-03-09 12:00:00.0",
                "type_id": "tipo 1"
            }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post("${Routes.PROFESSOR_PATH}/${Routes.ADD_SCHEDULE_BY_CODE_PROFESSOR_AND_DIARY_ID}", "01", 40L)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The diary is not founded")))
    }

    @Test
    @Sql("/queries/schedulecontrollertest/create_schedule_when_the_diary_is_not_assigned_test.sql")
    fun createScheduleWhenTheDiaryIsNotAssignedTest() {
        val activity = ActivityResponse(123456L, "actividad ejemplo", "descripcion ejemplo")

        Mockito.`when`(academicMsClient.findActivity("123456")).thenReturn(activity)

        val jsonBody =
            """{
                "day": "TUESDAY",
                "start_at": "2021-03-09 10:00:00.0",
                "ends_at": "2021-03-09 12:00:00.0",
                "type_id": "tipo 1"
            }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post("${Routes.PROFESSOR_PATH}/${Routes.ADD_SCHEDULE_BY_CODE_PROFESSOR_AND_DIARY_ID}", "01", 16L)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("The teachers diary is not assigned")
                )
            )
    }

    @Test
    @Sql("/queries/schedulecontrollertest/create_schedule_when_the_schedule_intersects_with_another_test.sql")
    fun createScheduleWhenTheScheduleIntersectsWithAnotherTest() {
        val activity = ActivityResponse(123456L, "actividad ejemplo", "descripcion ejemplo")

        Mockito.`when`(academicMsClient.findActivity("123456")).thenReturn(activity)

        val jsonBody =
            """{
                "day": "TUESDAY",
                "start_at": "2021-03-09 10:00:00 -5:00",
                "ends_at": "2021-03-09 12:00:00",
                "type_id": "tipo 1"
            }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.CREATE_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .post("${Routes.PROFESSOR_PATH}/${Routes.ADD_SCHEDULE_BY_CODE_PROFESSOR_AND_DIARY_ID}", "01", 20L)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("The Schedule is intersected with other schedule")
                )
            )
    }

    @Test
    @Sql("/queries/schedulecontrollertest/edit_schedule_test.sql")
    fun editScheduleTest() {

        val jsonBody =
            """{
                "day": "MONDAY",
                "start_at": "2021-03-09 10:00:00.0",
                "ends_at": "2021-03-09 12:00:00.0",
                "type_id": "tipo 2"
            }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.EDIT_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .put("${Routes.PROFESSOR_PATH}/${Routes.EDIT_SCHEDULE_BY_CODE_PROFESSOR_AND_SCHEDULE_ID}", "01", 27L)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val schedules = scheduleRepository.findAll().asSequence().toList()

        val scheduleToAssert = schedules.find { it.id == 27L }

        Assert.assertNotNull(scheduleToAssert)
        Assert.assertEquals(60L, scheduleToAssert?.diary?.id)
        Assert.assertEquals("01", scheduleToAssert?.diary?.professor?.code)
        Assert.assertEquals("monday", scheduleToAssert?.day.toString().toLowerCase())
        Assert.assertEquals("tipo 2", scheduleToAssert?.typeId)
    }

    @Test
    @Sql("/queries/schedulecontrollertest/edit_schedule_when_the_professor_not_exist_test.sql")
    fun editScheduleWhenTheProfessorNotExistTest() {

        val jsonBody =
            """{
                "day": "MONDAY",
                "start_at": "2021-03-09 10:00:00.0",
                "ends_at": "2021-03-09 12:00:00.0",
                "type_id": "tipo 2"
            }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.EDIT_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .put("${Routes.PROFESSOR_PATH}/${Routes.EDIT_SCHEDULE_BY_CODE_PROFESSOR_AND_SCHEDULE_ID}", "02", 21L)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The professor is not founded")))
    }

    @Test
    @Sql("/queries/schedulecontrollertest/edit_shedule_when_the_schedule_not_exist_test.sql")
    fun editScheduleWhenTheScheduleNotExistTest() {

        val jsonBody =
            """{
                "day": "MONDAY",
                "start_at": "2021-03-09 10:00:00.0",
                "ends_at": "2021-03-09 12:00:00.0",
                "type_id": "tipo 2"
            }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.EDIT_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .put("${Routes.PROFESSOR_PATH}/${Routes.EDIT_SCHEDULE_BY_CODE_PROFESSOR_AND_SCHEDULE_ID}", "01", 29L)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The schedule is not founded")))
    }

    @Test
    @Sql("/queries/schedulecontrollertest/edit_schedule_when_the_teacher_is_not_assigned_schedule_test.sql")
    fun editScheduleWhenTheTeacherIsNotAssignedScheduleTest() {

        val jsonBody =
            """{
                "day": "MONDAY",
                "start_at": "2021-03-09 10:00:00.0",
                "ends_at": "2021-03-09 12:00:00.0",
                "type_id": "tipo 2"
            }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.EDIT_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .put("${Routes.PROFESSOR_PATH}/${Routes.EDIT_SCHEDULE_BY_CODE_PROFESSOR_AND_SCHEDULE_ID}", "02", 55L)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The teacher is not assigned the schedule")))
    }

    @Test
    @Sql("/queries/schedulecontrollertest/edit_schedule_when_the_schedule_intersects_with_another_test.sql")
    fun editScheduleWhenTheScheduleIntersectsWithAnotherTest() {
        val jsonBody =
            """{
                "day": "MONDAY",
                "start_at": "2021-03-08 11:00:00",
                "ends_at": "2021-03-08 12:00:00",
                "type_id": "tipo 1"
            }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.EDIT_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .put("${Routes.PROFESSOR_PATH}/${Routes.EDIT_SCHEDULE_BY_CODE_PROFESSOR_AND_SCHEDULE_ID}", "01", 41L)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("The Schedule is intersected with other schedule")
                )
            )
    }

    @Test
    @Sql("/queries/schedulecontrollertest/editScheduleWithFoundActivityTest.sql")
    fun editScheduleWithFoundActivityTest() {
        Mockito.`when`(academicMsClient.findActivity("1")).thenReturn(ActivityResponse(1L, "Docencia directa", "Docencia directa"))

        val jsonBody =
            """{
                "day": "SUNDAY",
                "start_at": "2021-03-08 11:00:00",
                "ends_at": "2021-03-08 12:00:00",
                "type_id": "1"
            }"""

        val idProfessor: String = "0023"

        val idSchedule: Long = 1L

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.EDIT_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .put("${Routes.PROFESSOR_PATH}/${Routes.EDIT_SCHEDULE_BY_CODE_PROFESSOR_AND_SCHEDULE_ID}", idProfessor, idSchedule)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val schedules = scheduleRepository.findAll().toList()

        val scheduleToAssert = schedules.find { it.id == 1L }

        Assert.assertNotNull(scheduleToAssert)
        Assert.assertEquals(1L, scheduleToAssert?.diary?.id)
        Assert.assertEquals("0023", scheduleToAssert?.diary?.professor?.code)
        Assert.assertEquals("sunday", scheduleToAssert?.day.toString().toLowerCase())
        Assert.assertEquals("1", scheduleToAssert?.typeId)
    }

    @Test
    @Sql("/queries/schedulecontrollertest/editScheduleWithNotFoundActivityTest.sql")
    fun editScheduleWithNotFoundActivityTest() {
        Mockito.`when`(academicMsClient.findActivity("8")).thenThrow(createFeignNotFoundException())

        val jsonBody =
            """{
                "day": "SUNDAY",
                "start_at": "2021-03-08 11:00:00",
                "ends_at": "2021-03-08 12:00:00",
                "type_id": "8"
            }"""

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.EDIT_SCHEDULE_PROFESSOR)
        )

        val idProfessor: String = "0023"

        val idSchedule: Long = 1L

        val request = MockMvcRequestBuilders
            .put("${Routes.PROFESSOR_PATH}/${Routes.EDIT_SCHEDULE_BY_CODE_PROFESSOR_AND_SCHEDULE_ID}", idProfessor, idSchedule)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Activity Not Found")))
    }
    @Test
    @Sql("/queries/schedulecontrollertest/delete_schedule_test.sql")
    fun deleteScheduleTest() {

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.DELETE_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .delete("${Routes.PROFESSOR_PATH}/${Routes.DELETE_SCHEDULE_BY_SCHEDULE_ID}", 78L)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val schedules = scheduleRepository.findAll().asSequence().toList()

        val scheduleToAssert = schedules.find { it.id == 78L }
        val scheduleToAssertNotNull = schedules.find { it.id == 79L }

        Assert.assertNull(scheduleToAssert)

        Assert.assertNotNull(scheduleToAssertNotNull)

        Assert.assertEquals(78L, scheduleToAssertNotNull?.diary?.id)
    }

    @Test
    @Sql("/queries/schedulecontrollertest/delete_schedule_when_schedule_not_exist_test.sql")
    fun deleteScheduleWhenScheduleNotExistTest() {

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.DELETE_SCHEDULE_PROFESSOR)
        )

        val request = MockMvcRequestBuilders
            .delete("${Routes.PROFESSOR_PATH}/${Routes.DELETE_SCHEDULE_BY_SCHEDULE_ID}", 150L)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The schedule is not founded")))
    }
}
