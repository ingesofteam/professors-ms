INSERT INTO professor (code, biding, email, enabled, id_program, identification_number, last_name, "name", phone_number, "scale") VALUES ('01', 'Professor', 'email1@email.com',true, 0, '1010', 'Apellido 1', 'Nombre 1', 'telefono 1', 'scale 1');
INSERT INTO professor (code, biding, email, enabled, id_program, identification_number, last_name, "name", phone_number, "scale") VALUES ('02', 'Professor 2', 'email2@email.com',true, 0, '1012', 'Apellido 2', 'Nombre 2', 'telefono 2', 'scale 2');

INSERT INTO diaries (id, academic_period, code_professor) VALUES(20, 'academic_1', '01');
INSERT INTO diaries (id, academic_period, code_professor) VALUES(21, 'academic_2', '02');

INSERT INTO schedules (id, "day", ends_at, starts_at, type_id, id_diary) VALUES(25, 'MONDAY', '2021-03-08 12:00:00', '2021-03-08 10:00:00', '1', 20);
INSERT INTO schedules (id, "day", ends_at, starts_at, type_id, id_diary) VALUES(26, 'TUESDAY', '2021-03-09 12:00:00', '2021-03-09 10:00:00', '1', 20);